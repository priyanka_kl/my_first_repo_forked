from pymongo import MongoClient
import pandas as pd
client = MongoClient()
mydb = client["mydatabase"]
mycol = mydb["response_list"]
mylist = [
    {"response_id":1, "name" : "Priya"},
    {"response_id":2, "name" : "Peter"},
    {"response_id":3, "name" : "Sam"},
    {"response_id":4, "name" : "Rock"},
    {"response_id":5, "name" : "Reema"},
]
mycol.insert_many(mylist)
for x in mycol.find():
    print(x)

p = mycol.find()

df = pd.DataFrame(p, columns=['response_id' , 'name'])

df.to_csv(r'C:\Users\priyanka.kl.LITMUS\python-mongo\myfile1.csv', index = False, header = True)

print(df)