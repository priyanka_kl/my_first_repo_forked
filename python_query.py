from collections import OrderedDict
from pymongo import MongoClient
import dateutil
import bson
from datetime import datetime, timedelta
from bson import ObjectId
import pandas as pd

# to preserve the inserted order
client = MongoClient(document_class=OrderedDict)

# connecting to database and collection
mydb = client["litmus-db"]
mycol = mydb["customer_feedback_requests_archive"]

# Since ISODate is not available in python
# to dynamically always consider the date as past 6 months from the current date
startdatetime = datetime.now() - timedelta(6*365/12) 
enddatetime = datetime.now()


# storing the query results
p = mycol.find({
    "brand_id": ObjectId("5d92ec5d5536d75335163a18"),
    "created_date": {
        "$gte": startdatetime,
        "$lte": enddatetime
    },
        "$or": [
            {"trial_1": { "$exists": True }},
            {"trial_2": { "$exists": False }}]
}, {
    "tag_private_row_number": 1,
    "created_date": 1
})

# converting the results into a structured form
df = pd.DataFrame(p, columns=['_id' , 'created_date','tag_private_row_number'])

# writting the data into the file
df.to_csv(r'C:\Users\priyanka.kl.LITMUS\python-mongo\myfile2.csv', index = False, header = True)

# printing the data in console as well
print(df)


